const fs = require("fs");
const path = require("path");

// const getUsers = (path, callback) => {
//     fs.readFile(path, {encoding: "utf-8"}, (error, content) => {
//         if(!error){            
//             callback(content);
//         }else{
//             callback(new Error("Hubo error al tratar de leer el archivo"));
//         }
//     });
// }

const getUsers = (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, {encoding: "utf-8"}, (error, content) => {
            if(!error){            
                resolve(content);
            }else{
                reject(new Error("Hubo error al tratar de leer el archivo"));
            }
        });
    });
}



const usersFile = path.join(__dirname, "db_users.json");

// getUsers(usersFile, (content) => {
//     console.log(content);
// });

// getUsers(usersFile).then(content => {
//     console.log(content);
// }).catch(error => {
//     console.log(error);
// });

( async () => {
    try{
        let content = await getUsers(usersFile);
        console.log(content);
    }catch(error){
        console.log(error);
    }
})();
