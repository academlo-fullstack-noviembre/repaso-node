const express = require("express");
const path = require("path");

const app = express();

//Middlewares Intermediario entre la petición del cliente
//y la respuesta que le vamos a dar al cliente

//GET, POST, PUT, PATCH, DELETE
app.use("/public", express.static("public"));
app.use("/usuarios", express.static("users"));
app.use(express.static("assets"));

app.get("/", (peticion, respuesta) => {
    const indexFile = path.join(__dirname, "index.html");
    respuesta.sendFile(indexFile);
});

app.get("/contacto", (peticion, respuesta) => {
    const contactFile = path.join(__dirname, "contacto.html");
    respuesta.sendFile(contactFile);
});


app.listen(8000, () => {
    console.log("Servidor escuchando sobre el puerto 8000");
});